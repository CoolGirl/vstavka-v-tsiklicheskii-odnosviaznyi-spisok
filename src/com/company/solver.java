package com.company;

import java.io.*;
import java.util.StringTokenizer;

public class solver {

    public static void main(String[] args) throws IOException {
        // write your code here
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

        PrintWriter out = new PrintWriter(new OutputStreamWriter(System.out));

        StringTokenizer str;

        str = new StringTokenizer(in.readLine());

        if (str.hasMoreTokens()) {
            List myList = new List();
            while (str.hasMoreTokens()) {
                myList.Insert(Integer.parseInt(str.nextToken()));
            }
            myList.printElements();
            out.flush();
        }else{
            System.out.println("Too few args.");
        }
    }

    static class List {

        private int value;

        private List next;

        public void printElements(){
            List n = this;
            do {
                System.out.println(n.value+ " ");
                n = n.next;
            } while(n!=this);
        }
        public void Insert(int newValue) {
             if (next == null) {
                 value = newValue;
                 next = this;
             }
             else{
                 List current = new List();
                 current.value = newValue;
                 List n = this;
                 do{
                     Boolean moreOrEqual = newValue>=n.value;
                     Boolean lessOrEqual = newValue<=n.next.value;
                     if (moreOrEqual && lessOrEqual || ((n.next.value<n.value || this==next) && (moreOrEqual || lessOrEqual))){
                         current.next = n.next;
                         n.next = current;
                         return;
                     }
                     n= n.next;
                 }while(n!= this);
                 try {
                     throw new BadListException();
                 } catch (BadListException e) {
                     System.out.println("Bad list.");
                 }
             }
        }

        private class BadListException extends Throwable {
        }
    }
}
